# Vehicle Sales Flask API

## Set up the environment

```bash
# from inside the root directory...
virtualenv env
. env/bin/activate
pip install -r requirements.txt
```

## Start the API

```bash
# Set up the database
python api/manage.py db init
python api/manage.py db migrate
# Run the API
python api/run.py
```

## Use the API

```bash
# Upload bulk data from JSON file
curl -X POST -H "Content-Type: application/json" -d @data.json localhost:5000/record/upload
# Create a new sales record
curl -X POST -H "Content-Type: application/json" -d '{"vin":"13345678910181215","make":"honda","model":"accord","year":"2000","price":2300,"buyer":"George Washington","seller":"Honda Dealership","sale_date":"02/18/2018"}' localhost:5000/record
# Get all records
curl -X GET localhost:5000/record
# Get record by sale ID
curl -X GET localhost:5000/record/1
# Get all records filter by make and model, ordered by price
curl -X GET localhost:5000/record?make=honda&model=accord&order_by=price
# Get N most recent sale prices for vin
curl -X GET localhost:5000/record?vin=12345678910111213&order_by=sale_prce&limit=1
```

## TODO:

* Flexibility - the API querystrings are **case sensitive** (make=Honda and make=honda return different results).
* Security - Add a JWT authentication system to restrict access to routes.  
* Scalability - Using a vehicle table (pk=vin) could improve the data and reduce some redundant data.  If the API experiences heavy traffic, could use nginx to lighten the load.
* Data Integrity - Add custom validation to make sure a VIN is 17 digits long, and a car year is reasonable.  Most fields are being checked based on marshmallow schema.  Create table of known makes, models, and dealerships to verify against.
* Auditability - To track the source of any incoming data as well as the source of any searches, we could implement an access_log table that logs each time the API is used, and the authentication token/user credentials used to search it.
