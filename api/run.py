#!/usr/bin/env python
"""
Run the API
"""
from flask import Flask
from models import db
from app import api_bp

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    app.register_blueprint(api_bp, url_prefix='')
    with app.app_context():
        db.init_app(app)
        db.create_all()
    return app

if __name__ == '__main__':
    APP = create_app('settings')
    APP.run(debug=True)
