#!/usr/bin/env python
"""
Define API and register routes
"""
from flask import Blueprint
from flask_restful import Api
from resources import VehicleSalesRecordResource, FileUploadResource

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Routes
api.add_resource(FileUploadResource, '/record/upload')
api.add_resource(VehicleSalesRecordResource, '/record', '/record/<int:record_id>')
