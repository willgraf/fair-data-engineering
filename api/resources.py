#!/usr/bin/env python
"""
Flask-RESTful resource definitions for each route.
Each resource defines the HTTP methods allowed for it.
"""
from datetime import datetime
from flask import request
from flask_restful import Resource
from sqlalchemy import desc, asc
from models import db, VehicleSalesRecord, VehicleSalesRecordSchema

records_schema = VehicleSalesRecordSchema(many=True)
record_schema = VehicleSalesRecordSchema()

class VehicleSalesRecordResource(Resource):
    """
    GET to return all records
    POST to create a single record
    """
    def get(self, record_id=None):
        if record_id:
            record = VehicleSalesRecord.query.get(record_id)
            result = record_schema.dump(record).data
        else:
            filter_args = ['make', 'model', 'year', 'buyer', 'seller', 'vin']
            filters = {fa: request.args.get(fa) for fa in filter_args if request.args.get(fa)}

            order_by = request.args.get('order_by', 'sale_date')
            if order_by not in {'sale_date', 'price'}:
                return {'error': '{} is not a valid order_by type'.format(order_by)}, 422
            order_by = VehicleSalesRecord.sale_date if order_by == 'sale_date' else VehicleSalesRecord.price

            sort = request.args.get('sort', 'desc')
            if sort not in {'asc', 'desc'}:
                return {'error': '{} is not a valid sort type'.format(sort)}, 422

            limit = request.args.get('limit')
            if limit and not limit.isdigit():
                return {'error': 'limit {} must be an integer'.format(limit)}, 422

            sort = desc if sort == 'desc' else asc

            if filters:
                records = VehicleSalesRecord.query \
                            .filter_by(**filters) \
                            .order_by(sort(order_by)).limit(limit)
            else:
                records = VehicleSalesRecord.query.order_by(sort(order_by)).limit(limit).all()
            result = records_schema.dump(records).data
        return {'data': result}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No input data provided'}, 400

        # Validate and deserialize input
        data, errors = record_schema.load(json_data)
        if errors:
            return errors, 422

        record = VehicleSalesRecord(**data)

        db.session.add(record)
        db.session.commit()

        result = record_schema.dump(record).data

        return {'status': 'success', 'data': result}, 201

class FileUploadResource(Resource):
    """
    POST a file to create many records at once
    """
    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No input data provided'}, 400

        # Validate and deserialize input
        data, errors = records_schema.load(json_data)
        if errors:
            return errors, 422

        records = [VehicleSalesRecord(**d) for d in data]

        db.session.bulk_save_objects(records)
        db.session.commit()

        result = records_schema.dump(records).data

        return {'status': 'success', 'data': result}, 201
