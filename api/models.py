#!/usr/bin/env python
"""
SqlAlchemy Model for Vehicle Sales Records
"""
from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
ma = Marshmallow()

class VehicleSalesRecord(db.Model):
    __tablename__ = 'sales_records'
    id = db.Column(db.Integer, primary_key=True)
    vin = db.Column(db.Integer, nullable=False)
    make = db.Column(db.String(250), nullable=False)
    model = db.Column(db.String(250), nullable=False)
    year = db.Column(db.Integer, nullable=False)
    price = db.Column(db.Integer, nullable=False)
    buyer = db.Column(db.String(250), nullable=False)
    seller = db.Column(db.String(250), nullable=False)
    sale_date = db.Column(db.Date, nullable=False)

    def __init__(self, vin, make, model, year, price, buyer, seller, sale_date):
        self.vin = vin
        self.make = make
        self.model = model
        self.year = year
        self.price = price
        self.buyer = buyer
        self.seller = seller
        self.sale_date = sale_date


class VehicleSalesRecordSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    vin = fields.Integer(required=True)
    make = fields.String(required=True, validate=validate.Length(1))
    model = fields.String(required=True, validate=validate.Length(1))
    year = fields.Integer(required=True)
    price = fields.Float(required=True)
    buyer = fields.String(required=True, validate=validate.Length(1))
    seller = fields.String(required=True, validate=validate.Length(1))
    sale_date = fields.Date(required=True)
