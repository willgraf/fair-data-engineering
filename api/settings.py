#!/usr/bin/env python
"""
Configuration settings for the Flask App
"""
# TODO: Move any sensitive info to a .env file

SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
BUNDLE_ERRORS = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///app.db'
